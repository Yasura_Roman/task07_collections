package com.yasiuraroman.model;

import org.apache.logging.log4j.Logger;

public class BinaryTreeModel implements Model {

    MyBinaryTree tree;

    @Override
    public void newTree() {
        tree = new MyBinaryTree();
}

    @Override
    public <K extends Comparable<K>, V> void put(K key, V value) {
        tree.put(key,value);
    }

    @Override
    public <K extends Comparable<K>, V> V get(K key) {
        return (V) tree.get(key);
    }

    @Override
    public void print(Logger logger) {
        tree.print(logger);
    }

}
