package com.yasiuraroman.view;

public interface Printable {
    void print();
}
