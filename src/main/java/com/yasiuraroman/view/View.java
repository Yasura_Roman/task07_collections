package com.yasiuraroman.view;

import com.yasiuraroman.controller.Controller;
import com.yasiuraroman.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();

    public View() {
        controller = new ControllerImp() {
        };
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - New binary tree");
        menu.put("2", "  2 - put element");
        menu.put("3", "  3 - get element");
        menu.put("4", "  4 - print tree");
        menu.put("Q", "  Q - exit");

//        methodsMenu = new LinkedHashMap<>();
//        methodsMenu.put("1", this::pressButton1);
//        methodsMenu.put("2", this::pressButton2);
//        methodsMenu.put("3", this::pressButton3);
//        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1(){
        controller.newTree();
        logger.info("Create New Tree");
    }

    private void pressButton2() {
        int key;
        int value;
        logger.info("enter Key");
        try {
            key = input.nextInt();
            logger.info("enter Value");
            value = input.nextInt();
            controller.put(key,value);
        }catch (Exception e){
            logger.error("not valid");
        }

    }

    private void pressButton3() {
        int key;
        logger.info("enter Key;");
        try {
            key = input.nextInt();
            logger.info("enter Value");
            logger.info(controller.get(key).toString());
        }catch (Exception e){
            logger.error("not valid");
        }
    }

    private void pressButton4() {
        controller.print(logger);
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                for (Menu menu:Menu.values()
                     ) {
                    if (menu.getText().equals(keyMenu)){
                        menu.getPrintable().print();
                    }
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private enum Menu{
        ONE("1",new View()::pressButton1),
        TWO("2",new View()::pressButton2),
        TREE("3",new View()::pressButton3),
        FOUR("4",new View()::pressButton4);

        private String text;
        private Printable printable;

        Menu(String text,Printable printable){
            this.text = text;
            this.printable = printable;
        }

        public String getText() {
            return text;
        }

        public Printable getPrintable() {
            return printable;
        }
    }
}

