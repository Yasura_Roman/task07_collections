package com.yasiuraroman.controller;

import com.yasiuraroman.model.BinaryTreeModel;
import com.yasiuraroman.model.Model;
import org.apache.logging.log4j.Logger;

public class ControllerImp implements Controller {

    private Model model;

    public ControllerImp() {
        model = new BinaryTreeModel();
    }

    @Override
    public void newTree() {
        model.newTree();
    }

    @Override
    public <K extends Comparable<K>, V> void put(K key, V value) {
        model.put(key,value);
    }

    @Override
    public <K extends Comparable<K>, V> V get(K key) {
        return model.get(key);
    }

    @Override
    public void print(Logger logger) {
        model.print(logger);
    }
}
