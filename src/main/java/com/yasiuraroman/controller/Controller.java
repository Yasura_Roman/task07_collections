package com.yasiuraroman.controller;

import org.apache.logging.log4j.Logger;

public interface Controller {
    void newTree();
    <K extends Comparable<K>, V> void put(K key,V value);
    <K extends Comparable<K>, V> V get(K key);
    void print(Logger logger);
}
