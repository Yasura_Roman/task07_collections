package com.yasiuraroman.model;


import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyBinaryTree<K extends Comparable, V> implements Map<K,V> {

    private TreeNode root;
    private int size;
    private Collection<V> values;
    private Set<K> keys;
    private Set<Entry<K,V>> entries;

    public MyBinaryTree() {
        values = new ArrayList<>();
        keys = new HashSet<>();
        entries = new HashSet<>();
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public boolean containsKey(Object key) {
        return inOrderSearchForKey(root, key);
    }

    private boolean inOrderSearchForKey(TreeNode node, Object key) {
        if (node != null) {
            if (inOrderSearchForKey(node.leftChild, key)) {
                return true;
            }
            if (node.key.equals(key)) {
                return true;
            }
            if (inOrderSearchForKey(node.rightChild, key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return inOrderSearchForValue(root, value);
    }
    private boolean inOrderSearchForValue(TreeNode node, Object value) {
        if (node != null) {
            if (inOrderSearchForValue(node.leftChild, value)) {
                return true;
            }
            if (node.value.equals(value)) {
                return true;
            }
            if (inOrderSearchForValue(node.rightChild, value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        TreeNode currentNode = root;
        while (currentNode.key.compareTo(key) != 0) {
            if (currentNode.key.compareTo(key) > 0) {
                currentNode = currentNode.leftChild;
            } else {
                currentNode = currentNode.rightChild;
            }
            if (currentNode == null) {
                return null;
            }
        }
        return currentNode.value;
    }

    @Override
    public V put(K key, V value) {
        TreeNode entry = new TreeNode(key, value);
        this.keys.add(key);
        this.values.add(value);
        this.entries.add(entry);
        if (root == null) {
            this.root = entry;
            this.size++;
            return value;
        }
        TreeNode nodeForInsertion = getNodeForInsertion(root, key);
        if (nodeForInsertion.key.compareTo(key) > 0) {
            nodeForInsertion.leftChild = new TreeNode(key, value);
            this.size++;
        } else if (nodeForInsertion.key.compareTo(key) < 0) {
            nodeForInsertion.rightChild = entry;
            this.size++;
        } else {
            nodeForInsertion.setValue(value);
        }
        return value;
    }
    private TreeNode getNodeForInsertion(TreeNode startNode, K key) {
        TreeNode currentNode = startNode;
        while (currentNode.leftChild != null || currentNode.rightChild != null) {
            int nodesComparingResult = currentNode.key.compareTo(key);
            if (nodesComparingResult > 0) {
                if (currentNode.leftChild != null) {
                    currentNode = currentNode.leftChild;
                } else {
                    break;
                }
            } else if (nodesComparingResult < 0) {
                if (currentNode.rightChild != null) {
                    currentNode = currentNode.rightChild;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        return currentNode;
    }

    @Override
    public V remove(Object key) {
        TreeNode parentNode = root;
        TreeNode currentNode = root;
        boolean isLeftChild = true;
        V value = null;
        while (currentNode.key != key) {
            parentNode = currentNode;
            if (currentNode.key.compareTo(key) > 0) {
                isLeftChild = true;
                currentNode = currentNode.leftChild;
            } else {
                isLeftChild = false;
                currentNode = currentNode.rightChild;
            }
            if (currentNode == null) {
                return value;
            }
        }
        value = currentNode.value;
        this.values.remove(value);
        this.keys.remove(key);
        this.entries.remove(currentNode);
        if (currentNode.leftChild == null && currentNode.rightChild == null) {
            removeIfNodeHasNoChild(parentNode, currentNode, isLeftChild);
        } else if (currentNode.leftChild != null && currentNode.rightChild != null) {
            TreeNode successor = this.getSuccessor(currentNode);
            this.removeIfNodeHasTwoChild(parentNode, currentNode, successor, isLeftChild);
        } else {
            removeIfNodeHasOneChild(parentNode, currentNode, isLeftChild);
        }
        return value;
    }

    private TreeNode getSuccessor(TreeNode nodeForDeletion) {
        TreeNode successorParent = nodeForDeletion;
        TreeNode successor = nodeForDeletion;
        TreeNode currentNode = nodeForDeletion.rightChild;
        while (currentNode != null) {
            successorParent = successor;
            successor = currentNode;
            currentNode = currentNode.leftChild;
        }
        if (successor != nodeForDeletion.rightChild) {
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = nodeForDeletion.rightChild;
        }
        return successor;
    }

    private void removeIfNodeHasOneChild(
            TreeNode parent,
            TreeNode nodeForDeletion,
            boolean isLeftChild) {
        if (nodeForDeletion.leftChild != null) {
            if (isLeftChild) {
                parent.leftChild = nodeForDeletion.leftChild;
            } else {
                parent.rightChild = nodeForDeletion.leftChild;
            }
        } else {
            if (isLeftChild) {
                parent.leftChild = nodeForDeletion.rightChild;
            } else {
                parent.rightChild = nodeForDeletion.rightChild;
            }
        }
    }

    private void removeIfNodeHasTwoChild(
            TreeNode parentNode,
            TreeNode nodeForDeletion,
            TreeNode successor,
            boolean isLeftChild) {
        if (nodeForDeletion == root) {
            root = successor;
        } else if (isLeftChild) {
            parentNode.leftChild = successor;
        } else {
            parentNode.rightChild = successor;
        }
        successor.leftChild = nodeForDeletion.leftChild;
    }

    private void removeIfNodeHasNoChild(
            TreeNode parentNode,
            TreeNode nodeForRemoving,
            boolean isLeftChild) {
        if (nodeForRemoving == root) {
            this.root = null;
            size = 0;
        } else if (isLeftChild) {
            parentNode.leftChild = null;
        } else {
            parentNode.rightChild = null;
        }
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {
        this.root = null;
        this.size = 0;
    }

    @Override
    public Set<K> keySet() {
        return this.keys;
    }

    @Override
    public Collection<V> values() {
        return this.values;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return this.entries;
    }
    public void print(Logger logger) {
        inOrderPrint(root,logger);
    }

    private void inOrderPrint(TreeNode root, Logger logger) {
        if (root != null) {
            inOrderPrint(root.leftChild,logger);
            logger.info(root.key);
            inOrderPrint(root.rightChild,logger);
        }
    }

    private class TreeNode implements Entry<K, V> {
        private K key;
        private V value;
        private TreeNode leftChild;
        private TreeNode rightChild;
        public TreeNode(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            this.value = value;
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Entry){
                if(((Entry) obj).getKey().equals(this.key) == true){
                    return ((Entry) obj).getValue().equals(this.value);
                }
            }
            return false;
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }
}
